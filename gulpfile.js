const gulp = require('gulp'),
browserSync = require("browser-sync").create(),
rename = require('gulp-rename'),
sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer'),
imagemin = require('gulp-imagemin'),
clean = require('gulp-clean'),
del = require('del'),
htmlPartial = require('gulp-html-partial'),
rigger = require('gulp-rigger'),
uglify = require('gulp-uglify-es').default;

del.sync(['./app/**']);



gulp.task('clean-images', () => {
    return gulp.src('app/images/*', {read: false})
        .pipe(clean());
});

gulp.task('images', ['clean-images'], () => {
    gulp.src('src/img/**/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]).on('error', sass.logError))
        .pipe(gulp.dest('./app/img'))
        .pipe(browserSync.stream());
});

gulp.task('uploadImages', ['clean-images'], () => {
    gulp.src('src/upload/**/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]).on('error', sass.logError))
        .pipe(gulp.dest('./app/upload'))
        .pipe(browserSync.stream());
});


gulp.task('sass',  () => {
    gulp.src('src/scss/**/*.scss')
        .pipe(sass({
            outputStyle:
                'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());

  gulp.src('./src/scss/**/*.scss')
    .pipe(sass({
      outputStyle:
        'compressed'
    }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.stream());
});


gulp.task('clean-html', () => {
    return gulp.src('app/*.html', {read: false})
        .pipe(clean());
});

gulp.task('html', ['clean-html'], () => {
    gulp.src('src/*.html')
        .pipe(rigger())
        .pipe(htmlPartial({
            basePath: 'src/partials/',
            tagName: 'go'
        }).on('error', sass.logError))
        .pipe(gulp.dest('app/'))
        .pipe(browserSync.stream())
});

gulp.task('clean-js', () => {
    return gulp.src('app/*.js', {read: false})
        .pipe(clean());
});

gulp.task('compress', ['clean-js'], () => {
    gulp.src(['src/js/*.js', '!./src/js/*.min.js'])
        .pipe(rename({suffix: ".min"}))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'))
        .pipe(gulp.dest('src/js'))
        .pipe(browserSync.stream());
});

gulp.task('browsersync', (callback) => {
  browserSync.init({
    injectChanges: true,
    server: {
      baseDir: "./app"
    },
    logPrefix: "Frontend"
  });

  callback();
});

gulp.task('fonts', function() {
    gulp.src('src/fonts/**/*')
        .pipe(gulp.dest("app/fonts"))
        .pipe(browserSync.stream());
});

gulp.task('vendor', function() {
    gulp.src('src/vendor/**/*')
        .pipe(gulp.dest("app/vendor"))
        .pipe(browserSync.stream());
});

gulp.task('video', function() {
    gulp.src('src/video/**/*')
        .pipe(gulp.dest("app/video"))
        .pipe(browserSync.stream());
});

gulp.task('watch', () => {
    gulp.watch('src/scss/**/*.scss', () => {
        setTimeout(() => {
            gulp.start('sass');
        }, 500);
    });
    gulp.watch('src/partials/**/*.html', ['html']);
    gulp.watch('src/img/**/*', ['images']);
    gulp.watch('src/img/**/*', ['uploadImages']);
    gulp.watch('src/**/*.html', ['html']);
    gulp.watch('src/js/*.js', ['compress']);
    gulp.watch('src/vendor/**/*', ['vendor']);
    gulp.watch('src/fonts/**/*', ['fonts']);
    gulp.watch('src/video/**/*', ['video']);
});

gulp.task('default', ['watch','images','uploadImages', 'video', 'sass', "vendor", "fonts", 'html', 'compress', 'browsersync',]);






