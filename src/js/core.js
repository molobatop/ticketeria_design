(function(){
   
    // Media Query
    function mediaQuery(query, handler1, handler2){
        if (matchMedia) {
            const MOBILE_RES = window.matchMedia(`(${query})`);
            MOBILE_RES.addListener(WidthChange);
            WidthChange(MOBILE_RES);
          }
    
        function WidthChange(MOBILE_RES) {
            if (MOBILE_RES.matches) {
                handler1();
            } else {
                handler2();
            }
        }
    };
    
    // Burger !!!
    document.querySelector(".hamburger").onclick = function(){
        this.classList.toggle("is-active");
        document.querySelector(".mobile-menu").classList.toggle("active");
        // Обязательный класс, если мы хотим, чтобы логотип и бургер отображались
        document.querySelector(".header").classList.toggle("active-popup");
        document.querySelector("body").classList.toggle("disable-scroll");
    };

    // Drop menu
    document.querySelectorAll(".popup-menu__drop").forEach( item => {
        
        item.querySelector("a").onclick = function(e){
            e.preventDefault();

            if(item.classList.contains("active")){
                item.classList.remove("active");

                return;
            }
            document.querySelectorAll(".popup-menu__drop.active").forEach( otherItems => {
                otherItems.classList.remove("active");
            });

            item.classList.add("active");
        };
    });

    // Mobile Search
    const mainSearchInput = document.querySelector(".header__search-form-input");
    const mobileSearchWindow = document.querySelector(".search-popup");
    const mobileSearchInput = document.querySelector(".search-popup__input");
    const closeMobileSearch = document.querySelector(".search-popup__input-close");

    
    mainSearchInput.onclick = function(){
        mobileSearchWindow.classList.add("active");
        document.querySelector("body").classList.add("disable-scroll");
        mobileSearchInput.focus();

        mobileSearchInput.value = mainSearchInput.value;
    };
    closeMobileSearch.onclick = function(e){
        e.preventDefault();
        document.querySelector("body").classList.remove("disable-scroll");
        mobileSearchWindow.classList.remove("active");
    };


    document.querySelectorAll(".main-menu-drop").forEach(item => {
        item.onclick = (e)=> {
            e.preventDefault();
            if(item.classList.contains("active")){
                item.classList.remove("active");
                return;
            }
            document.querySelectorAll(".main-menu-drop.active").forEach( otherItems => {
                otherItems.classList.remove("active");
            });
            item.classList.add("active");
        }
    });


    document.querySelector(".location-inner").onclick = (e)=> {
        e.preventDefault();
        document.querySelector(".location-popup").classList.toggle("active");
        document.querySelector("body").classList.add("disable-scroll");
    };

    document.querySelectorAll(".close-location").forEach( item => {
        item.onclick = (e)=> {
            e.preventDefault();
            document.querySelector(".location-popup").classList.remove("active");
            document.querySelector("body").classList.remove("disable-scroll");
        }
    });

    document.querySelectorAll(".header__top-menu-drop").forEach(function(item){
        item.onclick = function(e){
            e.preventDefault();
            item.classList.toggle("active");
        };
    });


    document.querySelectorAll(".multiselector").forEach(item => {
        item.onclick = ()=> {
            item.classList.toggle("active");
        }
    })

})();